/**
 * @file
 * Themes custom functions collection.
 *
 */
 (function ($, Drupal) {
  // Drupal.behaviors.drowl_child_functions = {
  //   attach: function (context, settings) {

  //   }
  // };

  // Workaround: Ensure Drupal.drowl_child exists because in some situations
  // this file is loaded before drowl_child/drowl_child - nobody (TF&JP) knows why...
  if(!Drupal.drowl_child){
    Drupal.drowl_child = {};
  }
  // Create drowl_child sub-namespace:
  if(!Drupal.drowl_child.functions){
    Drupal.drowl_child.functions = {};
  }

  /**
   * Example function
   */
  // Drupal.drowl_child.functions.exampleFunction = function () {

  // };

})(jQuery, Drupal);
