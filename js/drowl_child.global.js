(function ($, Drupal) {
  Drupal.behaviors.drowl_child_global = {
    attach: function (context, settings) {

    },
  };
  Drupal.behaviors.drowl_child_global_ui_modifications = {
    attach: function (context, settings) {
    },
  };

  // Enable for #IE11 support if needed.
  Drupal.behaviors.drowl_child_fixes = {
    attach: function (context, settings) {

      // Set IE body class, see: https://stackoverflow.com/a/21712356/6266306
      // if (window.document.documentMode) {
      //   $('body:first', context).addClass('is-ie');
      // }

      // IE CSS Variables polyfill
      // if (!(window.CSS && CSS.supports('color', 'var(--primary)'))) {
      //   console.log('No CSS variables supported => load polyfill.');
      //   cssVars({
      //     include: 'link[rel=stylesheet],style'
      //   });
      // }

      // Add object fit polyfill for older browsers (IE11)
      // https://github.com/constancecchen/object-fit-polyfill
      // var polyfill_element = document.querySelectorAll('.media-slide__media--image img');
      // objectFitPolyfill(polyfill_element);
    }
  };

})(jQuery, Drupal);
