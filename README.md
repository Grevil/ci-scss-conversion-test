
# DROWL Child Theme

## Setup

1. Rename __gulpfile.variables.js preset to grulpfile.variables.js
2. Fill the variables inside
3. If you wish to have those variables file in your git repository, remove the file from .gitignore
4. Run npm install
5. Run gulp build

## Usage

### Local
Run gulp watch to start the local watcher

### Upload to webserver
Run gulp watchUpload to compile & upload the files
