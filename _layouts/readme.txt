Hint: Our default layouts are located in the drowl_layouts module. You can override them here using this folder structure:

- page_layouts:       Layouts for whole pages, eg. frontpage, product page, .. so specific pages / content types
                      which are no "landingpages" build with the core layout builder using multiple stacked layouts.
                      They are much more complicated and have many more regions like the simple stacked / unstacked grids.
- stacked:            Layouts which have multiple stacked regions defined.
                      Stacked layouts also have a hidden content area. This area holds fields you want to place
                      directly inside your node.html.twig.
- unstacked:          Layouts with just a single line. This is kinda important for the bricks module,
                      because one sub-brick of a layout-brick represents one region, sequencly. So on stacked layouts
                      the first brick will become the "top" region while the user awaits it will be the first
                      column instead.
- components:         Those are not classical layouts, those are components like cards oder media object to make them
                      avaialable for direct usage inside Drupals field manager. Hint: If you add an object layout
                      keep in mind, we also use the patterns module, so if there is any chance this object needs to be
                      reused in a not layout context, make it a pattern and add the pattern to the layout.

Inside those folders we seperate them by the number of columns. If a layout doesn't fit in those categories.

Example for the drowl_base.layout.yml:

node_detail_default:
  label: 'Node Detail Default'
  category: 'Customer Specific'
  path: templates/_layouts/customer_specific
  template: node-detail-default
  default_region: main_full
  icon_map:
    - [top]
    - [main_full]
    - [main, main, main_aside]
    - [bottom]
  regions:
    title:
      label: Title
    subline:
      label: Title subline (fields aligned next each other and likely smaller font-size)
    top:
      label: Top
    main:
      label: Main
    main_aside:
      label: Aside Main (next to "Main")
    main_full:
      label: Main (Viewport-Width)
    bottom:
      label: Bottom
    content:
      label: Hidden (custom template use)
