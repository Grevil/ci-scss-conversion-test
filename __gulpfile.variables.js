// =============================================================================
// ====================== Project Configuration ================================
// =============================================================================

var devmode             = false; // Effect: .min.js / .min.css files are uncompressed & expanded - sourcemaps are generated

// Folders & File regex patterns
const folderCss               = "css";
const folderSass              = "scss";
const sassRegex               = [folderSass + '/**/*.scss'];
const cssDistRegex            = [folderCss + '/**/*.min.css', folderCss + '/**/*.min.css.map'];
const folderJs                = "js";
const jsRegex                 = [folderJs + '/*.js', '!' + folderJs + '/**/*.min.js'];
const jsDistRegex             = [folderJs + '/**/*.min.js'];
const folderLayouts           = "_layouts";
const folderTemplates         = "templates";
const tplRegex                = [folderTemplates + '/**/*.twig', folderLayouts + '/**/*.twig'];
const configRegex             = ['*.yml', '*.php', '*.theme', 'gulpfile.js', 'config/**/*', '!frontend_libraries/**', '!node_modules/**'];
const folderFonts             = "fonts";
const fontsRegex              = ['fonts/**/*.ttf', 'fonts/**/*.woff', 'fonts/**/*.woff2', 'fonts/**/*.svg', 'fonts/**/*.eot'];
const imagesRegex             = ['**/*.png', '**/*.jpg', '**/*.gif', '**/*.svg', '**/*.webp', '!frontend_libraries/**', '!node_modules/**'];
const folderFrontendLibraries = "frontend_libraries";
const frontendLibrariesRegex  = ['frontend_libraries/**/*'];

var remotePathRelative  = '/themes/custom/drowl_child'; // Relative Path to the theme root directory

// (S)FTP connection data (defaults and also upload direction without --customer parameter)
var host                = 'ENTER_HOST';
var user                = 'ENTER_USER';
var port                = 22;
// var agent            = 'pageant'; // <== Windows
var agent               = process.env.SSH_AUTH_SOCK; // <== Linux / WSL
var agentForward        = true;
// Webroot example '/var/www/vhosts/example.com/httpdocs/drupal9/web'
var remotePath          = 'ENTER_WEBROOT' + remotePathRelative;

module.exports = {
  devmode: devmode,
  folderCss: folderCss,
  folderSass: folderSass,
  folderJs: folderJs,
  folderTemplates: folderTemplates,
  folderFonts: folderFonts,
  sassRegex: sassRegex,
  cssDistRegex: cssDistRegex,
  jsRegex: jsRegex,
  jsDistRegex: jsDistRegex,
  tplRegex: tplRegex,
  configRegex: configRegex,
  imagesRegex: imagesRegex,
  fontsRegex: fontsRegex,
  folderFrontendLibraries: folderFrontendLibraries,
  frontendLibrariesRegex: frontendLibrariesRegex,
  remotePathRelative: remotePathRelative,
  remotePath: remotePath,
  host: host,
  user: user,
  port: port,
  agent: agent,
  agentForward: agentForward
};
